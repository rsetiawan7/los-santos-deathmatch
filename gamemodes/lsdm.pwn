// SA-MP Core
#include <a_samp>

// Debug level. Comment line below to disable debugging.
#define _DEBUG 4

// Third-party includes
#include <YSI-Core/y_debug>
#include <YSI-Coding/y_hooks>
#include <YSI-Data/y_iterate>
#include <YSI-Coding/y_timers>
#include <YSI-Visual/y_commands>

// Enumerations
#include "lsdm/enumerations.inc"

// Variables
#include "lsdm/variables.inc"

// In-game stuffs
#include "lsdm/pickups.inc"
#include "lsdm/spawnpoints.inc"

// Commands (for debugging purposes)
#include "lsdm/commands.inc"

// Main Function
main()
{
  print("\n----------------------------------");
  print("   Los Santos Deathmatch\n");
  print("         Written by");
  print("         rsetiawan7");
  print("----------------------------------\n");
}

// Functions
#include "lsdm/functions.inc"

// Callbacks
#include "lsdm/callbacks.inc"

// Timers
#include "lsdm/timers.inc"